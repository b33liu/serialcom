﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using LiveCharts.Configurations;
using System.ComponentModel;
using System.Threading;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;

namespace SerialCom
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private string[] portNames;
        public SeriesCollection SeriesCollection { get; set; }
        public ChartValues<double> DataValues { get; set; }
        public ChartValues<MeasureModel> ChartValues { get; set; } // display all samples causing lag
        private ChartValues<MeasureModel> _fullChartValues;
        public Func<double, string> DateTimeFormatter { get; set; }
        public Func<double, string> YAxisFormatter { get; set; }
        public double AxisStep
        {
            get { return _axisStep; }
            set
            {
                _axisStep = value;
                OnPropertyChanged("AxisStep");
            }
        }
        private double _axisStep;
        public double AxisUnit { get; set; }
        private double _axisMax;
        private double _axisMin;
        private Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        private NotifyIcon NotifyIcon;
        public const int SampleLimit = 10000;
        private const int _plotLimit = 500;
        private int _timeSpan;
        private bool _ioError = false;


        public MainWindow()
        {
            InitializeComponent();
            InitiallizeUI();
            InitiallizeNotifyIcon();
            DataValues = new ChartValues<double> { };
            var mapper = Mappers.Xy<MeasureModel>()
                .X(model => model.DateTime.Ticks)
                .Y(model => model.Value);
            Charting.For<MeasureModel>(mapper);
            ChartValues = new ChartValues<MeasureModel>();
            _fullChartValues = new ChartValues<MeasureModel>();
            DateTimeFormatter = value => new DateTime((long)value).ToString("HH:mm:ss");
            YAxisFormatter = (x) => string.Format("{0:0.0}", x);



            AxisStep = TimeSpan.FromSeconds(1).Ticks;
            AxisUnit = TimeSpan.TicksPerSecond;
            SetAxisLimits(DateTime.Now);
            DataContext = this;

            string filePath = config.FilePath;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender == connectButton)
            {
                if (portSelect.SelectedIndex != -1)
                {

                    if (connectButton.Content.Equals("Connect"))
                    {
                        var portName = portSelect.SelectedItem.ToString();
                        SerialPortUtils.OpenClosePort(portName, 9600, 0);
                        connectButton.Content = "Disconnect";
                        portSelect.IsEnabled = false;
                    }
                    else
                    {
                        var portName = portSelect.SelectedItem.ToString();
                        SerialPortUtils.OpenClosePort(portName, 9600, 0);
                        connectButton.Content = "Connect";
                        portSelect.IsEnabled = true;
                    }
                    //Debug.WriteLine(SerialPortUtils.SerialPort.RtsEnable);
                }
            }
            if (sender == connectButton2)
            {
                if (portSelect2.SelectedIndex != -1)
                {

                    if (connectButton2.Content.Equals("Connect"))
                    {
                        var portName = portSelect2.SelectedItem.ToString();
                        SerialPortUtils.OpenClosePort(portName, 9600, 1);
                        connectButton2.Content = "Disconnect";
                        portSelect2.IsEnabled = false;
                    }
                    else
                    {
                        var portName = portSelect2.SelectedItem.ToString();
                        SerialPortUtils.OpenClosePort(portName, 9600, 1);
                        connectButton2.Content = "Connect";
                        portSelect2.IsEnabled = true;
                    }
                    //Debug.WriteLine(SerialPortUtils.SerialPort.RtsEnable);
                }
            }
            if (sender == plotTest)
            {
                Debug.Print("plot test");

                DataValues = new ChartValues<double>();
                var rand = new Random();
                for (int i = 0; i < 10000; i++)
                {
                    DataValues.Add(rand.Next(0, 10));
                }
                float period = (float)timeSpanSpinner.Value / _plotLimit;


                if (period <= 1)
                {
                    period = 1;
                }

                ChartValues.Clear();
                //Make sure the latest sample is plotted, so the loop start from the last element
                for (int i = DataValues.Count - 1; i >= 0; i = (int)Math.Round(i - period))
                {
                    ChartValues.Add(new MeasureModel
                    {
                        DateTime = new DateTime(i),
                        Value = DataValues[i]
                    });
                    if (ChartValues.Count >= _plotLimit)
                    {
                        break;
                    }
                }


            }
            if (sender == clearTextBox)
            {
                textBox1.Text = "";
            }
        }

        private void OnDropDownOpen(object sender, EventArgs e)
        {

            if (sender == portSelect)
            {
                portNames = SerialPortUtils.GetPortNames();
                portSelect.Items.Clear();
                foreach (string name in portNames)
                {
                    portSelect.Items.Add(name);
                }
            }
            if (sender == portSelect2)
            {
                portNames = SerialPortUtils.GetPortNames();
                portSelect2.Items.Clear();
                foreach (string name in portNames)
                {
                    portSelect2.Items.Add(name);
                }
            }
        }



        private void InitiallizeUI()
        {
            portNames = SerialPortUtils.GetPortNames();
            foreach (string name in portNames)
            {
                portSelect.Items.Add(name);
                portSelect2.Items.Add(name);
            }
            ReadAllSettings();
            //string defaultPort = ReadSetting("Port");
            //int portIndex = portSelect.Items.IndexOf(defaultPort);
            //portSelect.SelectedIndex = portIndex;
            textBox1.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            plotTest.Visibility = Visibility.Hidden;
            timeSpanSpinner.Maximum = SampleLimit;
            SerialPortUtils.win = this;
        }
        private void InitiallizeNotifyIcon()
        {
            NotifyIcon = new NotifyIcon();
            Debug.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
            Debug.WriteLine(Directory.GetCurrentDirectory());
            NotifyIcon.Icon = new Icon("peter.ico");
            NotifyIcon.Visible = true;
            NotifyIcon.Text = "Pressure Sensor";
            NotifyIcon.ContextMenuStrip = new ContextMenuStrip();
            ToolStripMenuItem openItem = new ToolStripMenuItem("Open");
            ToolStripMenuItem exitItem = new ToolStripMenuItem("Exit");
            openItem.Click += new EventHandler((sender, args) =>
              {
                  Visibility = Visibility.Visible;
              });
            exitItem.Click += new EventHandler((sender, args) =>
            {
                System.Windows.Application.Current.Shutdown();
            });
            NotifyIcon.ContextMenuStrip.Items.Add(openItem);
            NotifyIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            NotifyIcon.ContextMenuStrip.Items.Add(exitItem);
            NotifyIcon.MouseClick += (sender, args) =>
            {
                if (args.Button == MouseButtons.Left)
                {
                    Visibility = Visibility.Visible;
                }
            };

            //NotifyIcon.MouseMove += (sender, args) =>
            //{
            //    NotifyIcon.ShowBalloonTip(3000);
            //};
            //TaskbarIcon = (TaskbarIcon)FindResource("NotifyMenu");
            //TaskbarIcon.MenuActivation = PopupActivationMode.RightClick;

        }

        public void UpdateData(string dataString)
        {
            string[] values = dataString.Split(',');
            // check h2o sensor first
            if (values[0].Equals("h2o"))
            {
                try
                {
                    double steamPressure = Convert.ToDouble(values[1], CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    return;
                }
                DateTime currentTime = DateTime.Now;
                string currentTimeString = currentTime.ToString("yyyy-MM-dd HH\\:mm\\:ss");
                if (!textBox1.Text.Equals("")) //clear text box at midnight
                {
                    if (!currentTimeString.Substring(0, 10).Equals(textBox1.Text.Substring(0, 10)))
                    {
                        textBox1.Text = "";
                    }
                }

                string timeAndData = currentTimeString + ", " + dataString;
                textBox1.AppendText(timeAndData);
                if (autoScroll.IsChecked == true)
                {
                    textBox1.ScrollToEnd();

                }
                string fileName = currentTime.ToString("yyyy-MM-dd");
                try
                {
                    using (StreamWriter streamWriter = new StreamWriter("C:\\pressureLog\\h2o\\" + fileName + ".csv", true))
                    {
                        streamWriter.Write(timeAndData);
                    }
                    _ioError = false;
                }
                catch (IOException ex)
                {
                    if (!_ioError)
                    {
                        _ioError = true;
                        Task.Factory.StartNew(() =>
                        {
                            System.Windows.MessageBox.Show("Log file is occupied");
                        });
                    }
                }
            }
            else // process original sensor 
            {
                string pressureString = values[0];
                DateTime currentTime = DateTime.Now;
                string currentTimeString = currentTime.ToString("yyyy-MM-dd HH\\:mm\\:ss");
                if (!textBox1.Text.Equals("")) //clear text box at midnight
                {
                    if (!currentTimeString.Substring(0, 10).Equals(textBox1.Text.Substring(0, 10)))
                    {
                        textBox1.Text = "";
                    }
                }

                string timeAndData = currentTimeString + ", " + dataString;
                textBox1.AppendText(timeAndData);

                if (autoScroll.IsChecked == true)
                {
                    textBox1.ScrollToEnd();

                }
                string fileName = currentTime.ToString("yyyy-MM-dd");
                try
                {
                    using (StreamWriter streamWriter = new StreamWriter("C:\\pressureLog\\" + fileName + ".csv", true))
                    {
                        streamWriter.Write(timeAndData);
                    }
                    _ioError = false;
                }
                catch (IOException ex)
                {
                    if (!_ioError)
                    {
                        _ioError = true;
                        Task.Factory.StartNew(() =>
                        {
                            System.Windows.MessageBox.Show("Log file is occupied");
                        });
                    }
                }
                try
                {
                    double pressure = Convert.ToDouble(pressureString, CultureInfo.InvariantCulture);
                    DataValues.Add(pressure);
                    _fullChartValues.Add(new MeasureModel
                    {
                        DateTime = currentTime,
                        Value = pressure
                    });
                }
                catch (FormatException ex)
                {
                    return;
                }
                ExtractPlotData();


                //ChartValues.Add(new MeasureModel
                //{
                //    DateTime = currentTime,
                //    Value = pressure
                //});
                SetAxisLimits(currentTime);

                //if (ChartValues.Count > SampleLimit)
                //{
                //    ChartValues.RemoveAt(0);
                //}
                if (_fullChartValues.Count > SampleLimit)
                {
                    _fullChartValues.RemoveAt(0);
                }
            }

        }

        private void ExtractPlotData()
        {


            float period = (float)(timeSpanSpinner.Value ?? 10) / _plotLimit;

            if (period <= 1)
            {
                period = 1;
            }

            ChartValues.Clear();
            //Make sure the latest sample is plotted, so the loop start from the last element
            for (int i = _fullChartValues.Count - 1; i >= 0; i = (int)Math.Round(i - period))
            {
                ChartValues.Add(_fullChartValues[i]);
                if (ChartValues.Count >= _plotLimit)
                {
                    break;
                }
            }



        }

        private void SetAxisLimits(DateTime now)
        {
            AxisMax = now.Ticks + TimeSpan.FromSeconds(0).Ticks; // lets force the axis to be 1 second ahead
            int timeSpan = timeSpanSpinner.Value ?? 10;
            AxisMin = now.Ticks - TimeSpan.FromSeconds(timeSpan).Ticks; // and 10 seconds behind
            AxisStep = TimeSpan.FromSeconds(Math.Round((double)timeSpan / 10)).Ticks;
        }



        public double AxisMax
        {
            get { return _axisMax; }
            set
            {
                _axisMax = value;
                OnPropertyChanged("AxisMax");
            }
        }

        public double AxisMin
        {
            get { return _axisMin; }
            set
            {
                _axisMin = value;
                OnPropertyChanged("AxisMin");
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        #endregion

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (portSelect.SelectedItem != null)
            {
                AddUpdateAppSettings("Port", portSelect.SelectedItem.ToString());
            }
            if (portSelect2.SelectedItem != null)
            {
                AddUpdateAppSettings("Port2", portSelect2.SelectedItem.ToString());
            }
            AddUpdateAppSettings("PlotTimeSpan", timeSpanSpinner.Value.ToString());
            e.Cancel = true;
            //WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;
            //this.ShowInTaskbar = false;
        }

        private void OnDropDownClosed(object sender, EventArgs e)
        {

            //if (sender == portSelect)
            //{
            //    if (portSelect.SelectedItem != null)
            //    {
            //        AddUpdateAppSettings("Port", portSelect.SelectedItem.ToString());
            //    }
            //}
        }

        private void AddUpdateAppSettings(string key, string value)
        {
            var settings = config.AppSettings.Settings;
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
        }

        private void ReadAllSettings()
        {
            var appSettings = ConfigurationManager.AppSettings;

            if (appSettings.Count == 0)
            {
                Console.WriteLine("AppSettings is empty.");
            }
            else
            {
                foreach (var key in appSettings.AllKeys)
                {
                    if (key.Equals("Port"))
                    {
                        //_defaultPort = appSettings[key];
                        string defaultPort = appSettings[key];
                        int portIndex = portSelect.Items.IndexOf(defaultPort);
                        portSelect.SelectedIndex = portIndex;
                    }
                    else if (key.Equals("Port2"))
                    {
                        //_defaultPort = appSettings[key];
                        string defaultPort = appSettings[key];
                        int portIndex = portSelect2.Items.IndexOf(defaultPort);
                        portSelect2.SelectedIndex = portIndex;
                    }
                    else if (key.Equals("PlotTimeSpan"))
                    {
                        int timeSpan = Int32.Parse(appSettings[key]);
                        timeSpanSpinner.Value = timeSpan;
                    }
                }
            }
        }


        private void OnExitClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void OnOpenClick(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Visible;
        }
    }
}
