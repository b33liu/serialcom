﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace SerialCom
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Debug.WriteLine(Directory.GetCurrentDirectory());
            SplashScreen s = new SplashScreen("pv.png");

            s.Show(false,true);
            s.Close(new TimeSpan(0, 0, 3));
            base.OnStartup(e);
        }
    }
}
