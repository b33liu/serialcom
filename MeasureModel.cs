﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerialCom
{
    public class MeasureModel
    {
        public DateTime DateTime { get; set; }
        public double Value { get; set; }
    }
}
