﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO.Ports;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SerialCom
{
    class SerialPortUtils
    {
        public static string[] GetPortNames()
        {
            return SerialPort.GetPortNames();
        }

        public static ArrayList SerialPorts = new ArrayList(2)
        {
            new SerialPort(),new SerialPort(),new SerialPort(),
        };
        public static ArrayList ReceiveBuffer = new ArrayList(2)
        {
            "",""
        };
        public static MainWindow win;
        

        public static SerialPort OpenClosePort(string comName, int baud, int portIndex)
        {
            SerialPort port = (SerialPort)SerialPorts[portIndex];
            //if (portIndex >= SerialPorts.Count)
            //{
            //    port = new SerialPort();
            //    SerialPorts.Add(port);
            //    ReceiveBuffer.Add("");
            //} else
            //{
            //    port = (SerialPort)SerialPorts[portIndex];
            //}

            if (port == null || !port.IsOpen)
            {
                port = new SerialPort
                {
                    //串口名称
                    PortName = comName,
                    //波特率
                    BaudRate = baud,
                    //数据位
                    DataBits = 8,
                    //停止位
                    StopBits = StopBits.One,
                    //校验位
                    Parity = Parity.None
                };

                port.RtsEnable = true;
                port.DtrEnable = true;
                //打开串口
                port.Open();
                //串口数据接收事件实现

                port.DataReceived += new SerialDataReceivedEventHandler(ReceiveData);

                SerialPorts[portIndex] = port;

                return port;
            }
            //串口已经打开
            else
            {
                port.Close();
                return port;
            }
        }

        public static void ReceiveData(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort _SerialPort = (SerialPort)sender;
            int index = SerialPorts.IndexOf(_SerialPort);
            int _bytesToRead = _SerialPort.BytesToRead;
            byte[] recvData = new byte[_bytesToRead];

            _SerialPort.Read(recvData, 0, _bytesToRead);
            string recvString = Encoding.ASCII.GetString(recvData);
            ReceiveBuffer[index] += recvString;
            Regex rx = new Regex(".+\r\n");
            MatchEvaluator matchEvaluator = new MatchEvaluator(MatchNReplace);
            ReceiveBuffer[index] = rx.Replace((string)ReceiveBuffer[index], matchEvaluator);


        }

        public static string MatchNReplace(Match match)
        {
            PrintString(match.Value);
            //Debug.WriteLine(match.Value);
            
            return "";
        }

        private static void PrintString(string s)
        {

            
            win.Dispatcher.Invoke(() =>
            {

                win.UpdateData(s);

            });
        }

        public static bool SendData(byte[] data, SerialPort port)
        {

            if (port != null && port.IsOpen)
            {
                port.Write(data, 0, data.Length);
                Debug.WriteLine("发送数据：" + data);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
